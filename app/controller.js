/**
 * Main application controller
 *
 * You can use this controller for your whole app if it is small
 * or you can have separate controllers for each logical section
 * 
 */
;(function() {

  angular
    .module('wave-form-player')
    .controller('MainController', MainController);

  MainController.$inject = ['$rootScope', '$timeout', 'uuid'];

  function MainController($rootScope, $timeout, uuid) {

    // 'controller as' syntax
    var main = this;
    main.uuid = uuid.new();
    main.allowScroll = false;
    main.message = '';
    main.status = 'init';
    main.filename = '';
    main.processResult = {};
    main.files = [];

    main.success = function($file, $message, $flow ){

    }
    main.startUpload = function($flow ){
      main.status = 'start';
      $flow.upload();
    }
    main.complete = function($flow, $file ){
      main.status = 'success';
      $.getJSON('http://phalcon.tools/php/process.php?file='+main.uuid+'&filename='+$flow.files[0].name.substring(36), function (response) {
        main.processResult = response;
        $timeout(function(){
          main.filename = 'http://phalcon.tools/php/MUSIC/'+main.uuid+'/'+main.uuid;
          main.status = 'complete';
          $rootScope.$broadcast('new-song',  'http://phalcon.tools/php/MUSIC/'+main.uuid+'/'+main.uuid);
          main.uuid = uuid.new();
        }, 100);
      });
    }
    main.reset = function($flow ){
      $flow.cancel();
      main.status = 'init';
    }

    main.doPlay = function(src ){
      $rootScope.$broadcast('new-song',  'http://phalcon.tools/php/MUSIC/'+src+'/'+src);
    }

    $.getJSON('http://phalcon.tools/php/list.php', function (response){
      main.files = response;
    });
    
  }



  $('#flow-container').show();

})();