<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$file = $_GET['file'];

class Autoloader {
    static public function loader($className) {
        $filename = realpath(dirname(__FILE__))."/" . str_replace('\\', '/', $className) . ".php";
        if (file_exists($filename)) {
            include($filename);
            if (class_exists($className)) {
                return TRUE;
            }
        }
        return FALSE;
    }
}
spl_autoload_register('Autoloader::loader');

use Pastelle\WaveForm;
$w = new Pastelle\WaveForm\Generator();
$dest = '/home/fabio/www/ng-modules/ng-waveform-player/php/MUSIC/'.$file.'/';
if(!is_dir($dest)){
    //Directory does not exist, so lets create it.
    mkdir($dest, 755, true);
}
$r = $w->generate('/home/fabio/www/ng-modules/ng-waveform-player/php/CHUNKS/'.$file, $dest, $file, $_GET['filename']);
echo json_encode($r);

