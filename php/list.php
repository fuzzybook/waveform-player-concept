<?php
/**
 * Created by PhpStorm.
 * User: fabio
 * Date: 6/3/16
 * Time: 5:26 PM
 */

$list = scandir('/home/fabio/www/ng-modules/ng-waveform-player/php/MUSIC/');
$music = array();

foreach($list as $dir){
    if($dir != '.' && $dir != '..'){
        $myfile = fopen('/home/fabio/www/ng-modules/ng-waveform-player/php/MUSIC/'.$dir.'/'.$dir.'.json', "r") or die("Unable to open file!");
        $data = fread($myfile,filesize('/home/fabio/www/ng-modules/ng-waveform-player/php/MUSIC/'.$dir.'/'.$dir.'.json'));
        $j = json_decode($data);
        $music[] = $j->info;
        fclose($myfile);
    }
}

echo json_encode($music);