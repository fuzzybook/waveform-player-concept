<?php
/**
 * Created by PhpStorm.
 * User: fabio
 * Date: 6/2/16
 * Time: 1:20 PM
 */
define('BASE_PATH', realpath(dirname(__FILE__)));
class Autoloader {
    static public function loader($className) {
        $filename = realpath(dirname(__FILE__))."/" . str_replace('\\', '/', $className) . ".php";
        if (file_exists($filename)) {
            include($filename);
            if (class_exists($className)) {
                return TRUE;
            }
        }
        return FALSE;
    }
}
spl_autoload_register('Autoloader::loader');

use \Flow;
use Pastelle\WaveForm;
use Pastelle\MediainfoParser;

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) && $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'POST') {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
    }
    exit;
}


$config = new \Flow\Config();
$config->setTempDir('./CHUNKS');
$file = new \Flow\File($config);

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if ($file->checkChunk()) {
        header("HTTP/1.1 200 Ok");
    } else {
        header("HTTP/1.1 204 No Content");
        return ;
    }
} else {
    if ($file->validateChunk()) {
        $file->saveChunk();
    } else {
        // error, invalid chunk upload request, retry
        header("HTTP/1.1 400 Bad Request");
        return ;
    }
}


$filename = substr($file->getfileName(), 0, 36);

if ($file->validateFile() && $file->save('./CHUNKS/'.$filename)) {
	echo $filename;
} else {
    //echo 'This is not a final chunk, continue to upload';
}
