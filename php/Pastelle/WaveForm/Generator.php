<?php
namespace Pastelle\WaveForm;


class Generator
{
    public function generate($file, $output, $fileName, $title)
    {
        $log = array();
        $f = sys_get_temp_dir().'/'.$fileName.'-wavelog.json';
        $logHandle = fopen($f, "w");
        fwrite($logHandle, '[');
        fwrite($logHandle, '"'.date("Y-m-d h:i:s").' '.microtime(true).' start: '.$fileName. ' '. $title.'",');
        fflush($logHandle);

        if (file_exists($file)) {
            $dest = $output.$fileName;
            copy($file, "{$dest}.wav");
            fwrite($logHandle, '"'.date("Y-m-d h:i:s").' '.microtime(true).' file copied ",');;
            fflush($logHandle);

            $tmpname = '/home/fabio/www/default/tmp/test11';
            exec("lame {$file} {$dest}.mp3 -V2 --vbr-new -q0 --lowpass 19.7");
            fwrite($logHandle, '"'.date("Y-m-d h:i:s").' '.microtime(true).' file compressed  -V2 --vbr-new -q0 --lowpass 19.7 ",');
            fflush($logHandle);

            exec("lame {$dest}.mp3 -m m -S -f -b 16 --resample 8 {$tmpname}.mp3 && lame -S --decode {$tmpname}.mp3 {$tmpname}.wav");
            fwrite($logHandle, '"'.date("Y-m-d h:i:s").' '.microtime(true).' file reduced",');
            fflush($logHandle);

            // delete temporary files
            unlink("{$tmpname}.mp3");

            $result = new \stdClass;
            $result->data = array();
            $result->info = new \stdClass;

            $p = new WavParser;
            $r = $p->ReadFile($file, true);
            $result->info->title = substr($title, 0, -4);
            $result->info->filename = $fileName;
            $result->info->mp3 = '-V2 --vbr-new -q0 --lowpass 19.7';
            $result->info->duration = $r['format']['duration'];
            $result->info->channels = $r['format']['numchannels'];
            $result->info->samplerate = $r['format']['samplerate'];
            $result->info->byterate = $r['format']['byterate'];
            $result->info->bitspersample = $r['format']['bitspersample'];
            $result->info->size = $r['data']['size'];

            fwrite($logHandle, '"'.date("Y-m-d h:i:s").' '.microtime(true).' file parsed",');
            fflush($logHandle);

            $r = $p->ReadFile("{$tmpname}.wav", true);
            $handle = fopen("{$tmpname}.wav", "r");

            $array = array();
            // TODO calculate ratio for max size
            $ratio = 80;
            $data_size = $r['data']['size'];
            fseek($handle, $r['data']['delta'], SEEK_CUR);

            $data_point = 0;
            while (!feof($handle) && $data_point < $data_size) {
                $tp = unpack('s', fread($handle, 2));
                $t = array_pop($tp);
                $array[] = (32736 + $t) / 65535;
                // skip bytes for memory optimization
                fseek($handle, $ratio, SEEK_CUR);
                $data_point += $ratio + 2;
            }
            fclose($handle);
            unlink("{$tmpname}.wav");
            fwrite($logHandle, '"'.date("Y-m-d h:i:s").' '.microtime(true).' file preprocessed",');
            fflush($logHandle);

            $data = $array;
            $l = count($data);
            $step = ceil($l / ($r['format']['duration'] * 10));


            $max = 0;
            $samples = 0;
            for ($i = 0; $i < $l; $i++) {
                if($i % $step == 0 && $i > 0){
                    $samples++;
                    $result->data[] = (float)number_format($max, 4);
                    $max = 0;

                }else{
                    if($max < $data[$i]){
                        $max = $data[$i];
                    }
                }

            }
            $result->info->samples = $samples;


            $handle = fopen($dest.'.json', "w");
            fwrite($handle, json_encode($result));
            fclose($handle);
            fwrite($logHandle, '"'.date("Y-m-d h:i:s").' '.microtime(true).' file processed"]');
            fclose($logHandle);
            return $result;
        } else{
            fwrite($logHandle, '"'.date("Y-m-d h:i:s").' '.microtime(true).' file not exists"]');
            fclose($logHandle);
            return false;
        }

    }
}



