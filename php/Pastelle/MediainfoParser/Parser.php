<?php
namespace Pastelle\MediainfoParser;

class Parser
{
    private $regex_section = "/^(?:(?:general|audio)(?:\s\#\d+?)*)$/i";
	public $fileName = '';
	public $format = '';
	public $formatProfile = '';
	public $fileSize = 0.0;
	public $duration = 0;
	public $bitRate = '';
	public $channels = '';
	public $streamSize = 0.0;


    public function parse($lines)
    {
        $output = [];
        foreach ($lines as $line) {
            $line = trim(strtolower($line));
            if (preg_match($this->regex_section, $line)) {
                $section = $line;
                $output[$section] = [];
            }
            if (isset($section)) {
                $output[$section][] = $line;
            }
        }
        if (count($output)) {
            $output = $this->parseSections($output);
        }
        
        return $this->formatOutput($output);
    }
    private function parseSections(array $sections)
    {
        $output = [];
        foreach ($sections as $key => $section) {
            $key_section = explode(' ', $key)[0];
            if (!empty($section)) {
                if ($key_section == 'general') {
                    $output[$key_section] = $this->parseProperty($section, $key_section);
                } else {
                    $output[$key_section][] = $this->parseProperty($section, $key_section);
                }
            }
        }
        return $output;
    }
    private function parseProperty($sections, $section)
    {
        $output = [];
        foreach ($sections as $info) {
            $property = null;
            $value = null;
            $info = explode(":", $info, 2);
            if (count($info) >= 2) {
                $property = trim($info[0]);
                $value = trim($info[1]);
            }
            if ($property && $value) {
                switch ($section) {
                    case 'general':
                        switch ($property) {
                            case "complete name":
                            case "completename":
                                $output['file_name'] = $this->stripPath($value);
                                break;
                            case "format":
                                $output['format'] = $value;
                                break;
                            case "duration":
                                $output['duration'] = $this->parseDuration($value);
                                break;
                            case "file size":
                            case "filesize":
                                $output['file_size'] = $this->parseFileSize($value);
                                break;
                            case "overall bit rate":
                            case "overallbitrate":
                                $output['bit_rate'] = $this->parseBitRate($value);
                                break;
                        }
                        break;
              
                    case 'audio':
                        switch ($property) {
                            case "codec id":
                            case "codecid":
                                $output['codec'] = $value;
                                break;
                            case "format":
                                $output['format'] = $value;
                                break;
                            case "bit rate":
                            case "bitrate":
                                $output['bit_rate'] = $this->parseBitRate($value);
                                break;
                            case "channel(s)":
                                $output['channels'] = $this->parseAudioChannels($value);
                                break;
                            case "title":
                                $output['title'] = $value;
                                break;
                            case "language":
                                $output['language'] = $value;
                                break;
                            case "format profile":
                            case "format_profile":
                                $output['format_profile'] = $value;
                                break;
                            case "stream size":
                            case "stream_size":
                                $output['stream_size'] = $this->parseFileSize($value);
                                break;
                        }
                        break;
                }
            }
        }
        return $output;
    }
    private function stripPath($string)
    {
        $string = str_replace("\\", "/", $string);
        $path_parts = pathinfo($string);
        return $path_parts['basename'];
    }
    private function parseFileSize($string)
    {
        $number = (float)$string;
        preg_match("/[KMGTPEZ]/i", $string, $size);
        if (!empty($size[0])) {
            $number = $this->computerSize($number, $size[0] . 'b');
        }
        return $number;
    }
    private function parseBitRate($string)
    {
        $string = str_replace(' ', '', $string);
        $string = str_replace('kbps', '', $string);
        return $string * 1000;
    }
    private function parseDuration($string)
    {
        //h= HOURS, mn= minutes, s= seconds, ms= milliseconds;
	$time = 0;
	$factors = ['h' => 3600000, 'mn' => 60000, 's' => 1000, 'ms' => 1];
	$tokens = explode(" ", $string, 2);
	$v = 0;
	foreach ($tokens as $token) {
		$v += preg_replace("/[^0-9,.]/", "", $token) * $factors[preg_replace("/[^a-z,.]/", "", $token)];
	}
         return $v;

    }
    private function parseWidthHeight($string)
    {
        return str_replace(array('pixels', ' '), null, $string);
    }
    private function parseAudioChannels($string)
    {
        $replace = array(
            ' ' => '',
            'channels' => 'ch',
            'channel' => 'ch',
            '1ch' => '1.0',
            '7ch' => '6.1',
            '6ch' => '5.1',
            '2ch' => '2.0'
        );
        return str_ireplace(array_keys($replace), $replace, $string);
    }
    private function formatOutput($data)
    {
	$o = new \stdClass;
	$o->fileName = '';
	$o->format = '';
	$o->fileSize = 0.0;
	$o->duration = 0;
	$o->bitRate = 0;
	$o->formatProfile = '';
	$o->channels = '';
	$o->streamSize = 0.0;
	if(!empty($data['general'])){
		$o->fileName = $data['general']['file_name'];
		$o->format = $data['general']['format'];
		$o->fileSize = $data['general']['file_size'];
		$o->duration = $data['general']['duration'];
		$o->bitRate = $data['general']['bit_rate'];
	}
	if(!empty($data['audio'][0])){
		$o->formatProfile = $data['audio'][0]['format_profile'];
		$o->channels = $data['audio'][0]['channels'];
		$o->streamSize = $data['audio'][0]['stream_size'];
	}
	return $o;
    }
    private function computerSize($number, $size)
    {
	$bytes = (float) $number;
        $size = strtolower($size);
        $factors = ['b' => 0, 'kb' => 1, 'mb' => 2, 'gb' => 3, 'tb' => 4, 'pb' => 5, 'eb' => 6, 'zb' => 7, 'yb' => 8];
        if (isset($factors[$size])) {
            return (float) number_format($bytes * pow(1024, $factors[$size]) , 2, '.', '');
        }
        return $bytes;
    }
}
