<?php
/**
 * Created by PhpStorm.
 * User: fabio
 * Date: 6/3/16
 * Time: 3:57 PM
 */
?>
<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" ng-csp="" ng-app="wave-form-player"> <!--<![endif]-->

<head>

    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>AngularJS Boilerplate</title>
    <meta name="description" content="Simple AngularJS WaveForm player">

    <!-- mobile meta -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

    <!--[if IE]>
    <link rel="shortcut icon" href="favicon.ico">
    <![endif]-->
    <!-- or, set /favicon.ico for IE10 win -->
    <meta name="msapplication-TileColor" content="#f01d4f">

    <!-- CSS -->
    <!-- build:css css/style.min.css -->
    <link rel="stylesheet" type="text/css" href="styles/style.css"/>
    <link rel="stylesheet" href="bower_components/angular-material/angular-material.css"/>
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.css"/>
    <!-- endbuild -->
</head>

<body class="main-wrapper" ng-cloak>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Main view for templates -->
<div class="container" layout="column" layout-fill ng-controller="MainController as main" >
    <div layout-padding>
        <md-whiteframe class="md-whiteframe-2dp" layout layout-fill layout-align="center center">
            <h1>Waveform player - concept tentative</h1>
        </md-whiteframe>
    </div>
    <div layout="row" layout-padding style="padding-bottom: 32px;" ng-cloak>
        <md-whiteframe class="md-whiteframe-2dp" layout layout-fill layout-align="center center">
            <wave-form-player src="main.filename" inner-color="'#cccccc'" outer-color="'transparent'"></wave-form-player>
        </md-whiteframe>
    </div>
    <div id="flow-container" layout-padding style="display: none">
        <md-whiteframe class="md-whiteframe-2dp" layout layout-fill layout-align="start center">


            <div flow-init="{target: 'http://phalcon.tools/php/upload.php'}"
                 flow-files-submitted="main.startUpload($flow)"
                 flow-file-success="main.success($file, $message, $flow)"
                 flow-complete="main.complete($flow, $file)"
                 flow-file-added="$file.name = main.uuid +  $file.name"
                 layout="row">

                <div ng-if="main.status == 'init'" ng-cloak>
                    <md-button class="md-raised  md-primary" flow-btn>Upload File</md-button>
                </div>

                <div layout="row" layout-fill layout-align="start space-between" ng-repeat="file in $flow.files" ng-if="main.status == 'start'" ng-cloak>
                    <md-button class="md-raised  md-warn" ng-click="file.cancel()">Cancel</md-button>
                    <div layout="column" layout-padding layout-fill>
                        <div>{{file.name.substring(36)}}</div>
                        <md-progress-linear md-mode="determinate" value="{{file.progress() * 100}}"></md-progress-linear>
                    </div>
                </div>

                <div ng-if="main.status == 'success'" layout-align="center center" layout="row">
                    <div><i class="fa fa-cog fa-spin fa-3x fa-fw"></i></div>
                    <div><span>Processing...</span></div>
                </div>

                <div layout="column" layout-fill layout-align="start space-between" ng-repeat="file in $flow.files" ng-if="main.status == 'complete'">
                    <div layout="column" layout-padding layout-fill>
                        <p><b>{{file.name.substring(36)}}</b><br>Sucessfully loaded</p>
                    <table>
                        <tr ng-repeat="(key, value) in main.processResult.info">
                            <td width="120px"><b>{{key}}</b></td>
                            <td>{{value}}</td>
                        </tr>
                    </table>
                        <md-button class="md-raised  md-primary" ng-click="main.reset($flow)">OK</md-button>
                    </div>
                </div>
            </div>
        </md-whiteframe>

        <md-content class="md-whiteframe-2dp"  style="height: 200px;">
            <table>
                <tr ng-repeat="(key, value) in main.files" ng-click="main.doPlay(value.filename)">
                    <td width="20%" align="right">{{value.duration | trackTime}}</td>
                    <td width="80%">{{value.title}}</td>
                </tr>
            </table>
        </md-content>
    </div>
</div>
</body>

<!-- Vendors -->
<!-- build:nonangularlibs js/nonangularlibs.js -->
<script type="text/javascript" src="js/nonangular/jquery-1.11.2.min.js"></script>
<!-- endbuild -->

<!-- Non-angular libraries -->
<!-- build:libs js/libs.js -->
<script type="text/javascript" src="js/nonangular/lodash.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="bower_components/OwlCarousel/owl-carousel/owl.carousel.min.js"></script>
<!-- endbuild -->

<!-- Angular external libraries for application -->
<!-- build:angularlibs js/angularlibs.js -->
<script type="text/javascript" src="bower_components/angular/angular.js"></script>
<script type="text/javascript" src="bower_components/angular-animate/angular-animate.js"></script>
<script type="text/javascript" src="bower_components/angular-aria/angular-aria.js"></script>

<script type="text/javascript" src="bower_components/angular-route/angular-route.js"></script>
<script type="text/javascript" src="bower_components/angular-sanitize/angular-sanitize.js"></script>
<script type="text/javascript" src="bower_components/angular-audio/app/angular.audio.js"></script>
<script type="text/javascript" src="bower_components/angular-material/angular-material.js"></script>
<script type="text/javascript" src="bower_components/flow.js/dist/flow.js"></script>
<script type="text/javascript" src="bower_components/ng-flow/dist/ng-flow.js"></script>

<!-- endbuild -->

<!-- Angular components -->
<!-- build:appcomponents js/appcomponents.js -->
<script type="text/javascript" src="app/app.js"></script>
<script type="text/javascript" src="app/config.js"></script>

<script type="text/javascript" src="components/directives/wave-form-player/wave-form-player.service.js"></script>
<script type="text/javascript" src="components/directives/wave-form-player/wave-form-player.directive.js"></script>
<!-- endbuild -->

<!-- Application sections -->
<!-- build:mainapp js/mainapp.js -->
<script type="text/javascript" src="app/controller.js"></script>
<!-- endbuild -->

<!-- templates from $templateCache -->
<!-- build:templates -->
<!-- endbuild -->

</html>
