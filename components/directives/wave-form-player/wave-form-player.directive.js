;(function () {

        'use strict';

        /**
         * Main navigation, just a HTML template
         * @author Jozef Butko
         * @ngdoc  Directive
         *
         * @example
         * <main-nav><main-nav/>
         *
         */
        angular
            .module('wave-form-player')
            .directive('waveFormPlayer', waveFormPlayer)
            .directive('draggable', draggable)
            .factory('uuid', uuid);

        function uuid(){
            var svc = {
                new: function() {
                    function _p8(s) {
                        var p = (Math.random().toString(16)+"000000000").substr(2,8);
                        return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
                    }
                    return _p8() + _p8(true) + _p8(true) + _p8();
                },

                empty: function() {
                    return '00000000-0000-0000-0000-000000000000';
                }
            };
            return svc;
        }

        draggable.$inject = ['$document'];

        function draggable($document) {
            return {
                restrict: 'A',
                link: function(scope, elm, attrs) {
                    var startX, startY, initialMouseX, initialMouseY;
                    elm.css({position: 'absolute'});

                    elm.bind('mousedown', function($event) {
                        startX = elm.prop('offsetLeft');
                        startY = elm.prop('offsetTop');
                        initialMouseX = $event.clientX;
                        initialMouseY = $event.clientY;
                        $document.bind('mousemove', mousemove);
                        $document.bind('mouseup', mouseup);
                        return false;
                    });

                    function mousemove($event) {
                        var dx = $event.clientX - initialMouseX;
                        var dy = $event.clientY - initialMouseY;
                        elm.css({
                            top:  startY + dy + 'px',
                            left: startX + dx + 'px'
                        });
                        return false;
                    }

                    function mouseup() {
                        $document.unbind('mousemove', mousemove);
                        $document.unbind('mouseup', mouseup);
                    }
                }
            };
        }
    
        waveFormPlayer.$inject = ['$rootScope', 'WaveFormPlayer', '$mdDialog'];

        function waveFormPlayer($rootScope, WaveFormPlayer, $mdDialog) {

            // Definition of directive
            var directiveDefinitionObject = {
                restrict: 'E',
                templateUrl: 'components/directives/wave-form-player/wave-form-player.html',
                scope: {
                    src: '=',
                    innerColor: '=',
                    outerColor: '=',
                    progressColor: '='
                },
                link: function (scope, elem, attrs, ctrl) {
                    scope.audio = null;
                    scope.progress = 0;
                    scope.info = {};
                    scope.scroll = false;
                    //=============================================================================================================
                    var container = elem.find('div')[0].children[0];
                    var samples = null;
                    var canvas = null;
                    var context = null;
                    var width = container.clientWidth;
                    var height = container.clientHeight;
                    var outerColor = outerColor || "transparent";
                    var innerColor = scope.innerColor || "#ff1b00";
                    var progressColor = scope.progressColor || "#FF4081";
                    var scroll = false;
                    var src = scope.src;
                    var prevProgress = -1;
                    var wave = [];
                    var slidePositionDown = false;
                    
                    if (container) {
                        canvas = createCanvas(container,  width, height);
                    } else {
                        throw "Either canvas or container option must be passed";
                    }

                    patchCanvasForIE(canvas);

                    context = canvas.getContext("2d");
                    width = parseInt(context.canvas.width, 10);
                    height = parseInt(context.canvas.height, 10);

                    $rootScope.$on('new-song', function(ev, src){
                        $.getJSON(src + '.json', function (_samples) {
                            samples = _samples;
                            update(_samples.data);
                            scope.audio = WaveFormPlayer.play(src + '.mp3')
                            scope.info = _samples.info;
                        });
                    })

                    scope.$watch('src', function (newval) {
                        if(newval != '') {
                            $.getJSON(newval + '.json', function (_samples) {
                                samples = _samples;
                                update(_samples.data);
                                scope.audio = WaveFormPlayer.play(newval + '.mp3')
                            });
                        }
                    });

                    scope.$watch('audio.progress', function (newval) {
                        if(newval) {
                            progress(newval);
                            scope.progress = newval * 100.0;
                        }
                    });

                    scope.$watch('progress', function (newval) {
                        if(newval) {
                            progress(newval / 100.0);
                        }
                    });

                    scope.$watch('scroll', function (newval) {
                        scroll  = newval;
                        changed();
                    });
                    
                    scope.slidePosition = function(mode){
                        if(!mode){
                            scope.audio.progress = scope.progress / 100.0;
                            scope.audio.play();
                            document.getElementById(scope.$id).focus();
                        }else{
                            scope.audio.pause();
                        }
                        changed();
                    }
                    
                    scope.doToglePlay = function(){
                        if(scope.audio.paused){
                            scope.audio.play();
                        }else{
                            scope.audio.pause();
                        }
                    }

                    scope.showVolume = function(ev) {
                        WaveFormPlayer.showVolume(ev);
                    };
                    
                    function scrollTo(pos) {
                        if (container.clientWidth < width) {
                            container.scrollLeft = ((width - container.clientWidth) / 100.0) * pos;
                        }
                    }

                    function progress(val) {
                        if (val) {
                            var pixels = Math.ceil(width * val);
                            if (prevProgress != pixels) {
                                prevProgress = pixels;
                                drawProgress(wave, pixels);
                                if (container.clientWidth < width) {
                                    if (pixels > container.clientWidth / 2) {
                                        container.scrollLeft = pixels - (container.clientWidth / 2);
                                    }
                                }
                            }
                        }
                    }

                    function changed() {
                        if(samples) {
                            update(samples.data);
                        }
                    }

                    function patchCanvasForIE(canvas) {
                        var oldGetContext;
                        if (typeof window.G_vmlCanvasManager !== "undefined") {
                            canvas = window.G_vmlCanvasManager.initElement(canvas);
                            oldGetContext = canvas.getContext;
                            return canvas.getContext = function (a) {
                                var ctx;
                                ctx = oldGetContext.apply(canvas, arguments);
                                canvas.getContext = oldGetContext;
                                return ctx;
                            };
                        }
                    }

                    function createCanvas(container, width, height) {
                        var canvas;
                        canvas = document.createElement("canvas");
                        container.appendChild(canvas);
                        canvas.width = width;
                        canvas.height = height;
                        return canvas;
                    }

                    function linearInterpolate(before, after, atPoint) {
                        return before + (after - before) * atPoint;
                    }

                    function interpolateArray(data, fitCount) {
                        var after, atPoint, before, i, springFactor, tmp = 0;
                        var newData = new Array();
                        springFactor = new Number((data.length - 1) / (fitCount - 1));
                        newData[0] = data[0];
                        i = 1;
                        while (i < fitCount - 1) {
                            tmp = i * springFactor;
                            before = new Number(Math.floor(tmp)).toFixed();
                            after = new Number(Math.ceil(tmp)).toFixed();
                            atPoint = tmp - before;
                            newData[i] = linearInterpolate(data[before], data[after], atPoint);
                            i++;
                        }
                        newData[fitCount - 1] = data[data.length - 1];
                        return newData;
                    }

                    function update(data) {
                        if (!scroll) {
                            context.canvas.width = container.clientWidth;
                            width = parseInt(context.canvas.width, 10);
                            data = interpolateArray(data, width);
                        } else {
                            context.canvas.width = data.length;
                            width = parseInt(context.canvas.width, 10);
                        }
                        container.scrollLeft = 0;
                        wave = data;
                        redraw(wave);
                    }

                    function drawProgress(data, progress) {
                        redraw(data);
                        var d, middle, x, y, h, i = 0;
                        context.fillStyle = progressColor;
                        middle = height / 2;

                        for (i = 0; i < progress; i++) {
                            d = data[i];
                            x = width / data.length;
                            y = height - height * d;
                            h = (middle - y) * 2;
                            context.clearRect(x * i, y, x, h );
                            context.fillRect(x * i, y, x, h);
                        }
                    }

                    function redraw(data) {
                        var d, middle, x, y, h, i = 0;
                        clear();
                        context.fillStyle = innerColor;
                        middle = height / 2;
                        context.fillRect(0, middle, data.length, 1);

                        
                        for (i = 0; i < data.length; i++) {
                            d = data[i];
                            x = width / data.length;
                            y = height - height * d;
                            h = (middle - y) * 2;
                            context.clearRect(x * i, y, x, h );
                            context.fillRect(x * i, y, x, h);
                        }
                    }

                    function clear() {
                        context.fillStyle = outerColor;
                        context.clearRect(0, 0, width, height);
                        return context.fillRect(0, 0, width, height);
                    }

                    //=============================================================================================================
                }
            }
            return directiveDefinitionObject;
        }
    })();