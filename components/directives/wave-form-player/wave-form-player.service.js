;(function () {


    'use strict';

    angular
        .module('wave-form-player')
        .factory('WaveFormPlayer', [
            '$rootScope', 'ngAudio', '$mdDialog', WaveFormPlayer
        ]);


    //////////////// factory


    function WaveFormPlayer($rootScope, ngAudio, $mdDialog) {
        var audio = null;
        var volume = 1;

        return {
            audio: audio,
            play: play,
            setVolume: setVolume,
            getVolume: getVolume,
            showVolume: showVolume
        };

        function play(url){
            if(audio){
                audio.stop();
                return audio = ngAudio.play(url);
            }else{
                return audio = ngAudio.play(url);
            }

        }

        function setVolume(_volume){
            volume = _volume;
            if(audio){
                audio.setVolume(volume);
            }
        }

        function getVolume(){
            return volume;
        }

        function showVolume(ev) {
            $mdDialog.show({
                controller: DialogController,
                template:
                '<md-dialog class="volume-dialog" aria-label="List dialog">' +
                '  <md-dialog-content layout-padding layout-fill layout="column">'+
                '    <div><h2>Volume</h2></div>' +
                '    <div layout="row" layout-align="center center">' +
                '    <div class="close-btn"><md-button aria-label="Close volume dialog" class="md-icon-button" id="volume-close-btn" ng-click="closeDialog()">'+
                '       <i class="fa fa-times" aria-hidden="true"></i>'+
                '    </md-button></div>' +
                '    <div class="volume-box" layout="row" layout-fill layout-align="center center"><i class="fa fa-volume-down" aria-hidden="true"></i>' +
                '    <div layout-padding flex><md-slider flex step="0.1" min="0" max="100" ng-model="volume" aria-label="red" id="red-slider" ng-mouseup="mouseup()"></md-slider></div>'+
                '    <i class="fa fa-volume-up" aria-hidden="true"></i>' +
                '    </div></div>' +
                '  </md-dialog-content>' +
                '</md-dialog>',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                hasBackdrop: false
            });
        };

        function DialogController($scope, $mdDialog, WaveFormPlayer) {
            $scope.volume = getVolume() * 100;
            $scope.$watch('volume', function (newval) {
                setVolume(newval / 100.0);
            });
            $scope.closeDialog = function(){
                $mdDialog.hide();
            }
            $scope.mouseup = function($element){
                document.getElementById("volume-close-btn").focus();;
            }
        }


    }


})();
